package com.zuitt.example;

import java.util.Scanner;

public class Exercise {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        System.out.println("Enter a year:");
        int year = userInput.nextInt();

        if(year % 4 == 0) {
            System.out.println(year + " is a leap year.");
        } else {
            System.out.println(year + " is not a leap year.");
        }
    }
}
