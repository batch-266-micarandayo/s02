package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        //Array
        int[] firstFivePrimeNumbers = new int[5];

        firstFivePrimeNumbers[0] = 2;
        firstFivePrimeNumbers[1] = 3;
        firstFivePrimeNumbers[2] = 5;
        firstFivePrimeNumbers[3] = 7;
        firstFivePrimeNumbers[4] = 11;

        System.out.println("The first prime number is: " + firstFivePrimeNumbers[0]);

        //ArrayList
        ArrayList<String> myFriends = new ArrayList<String>();

        myFriends.add("John");
        myFriends.add("Jane");
        myFriends.add("Chloe");
        myFriends.add("Zoey");

        System.out.println("My friends are: " + myFriends);

        //Hashmap
        HashMap<String, Integer> currentInventory = new HashMap<String, Integer>();

        currentInventory.put("toothpaste", 15);
        currentInventory.put("toothbrush", 20);
        currentInventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + currentInventory);
    }
}
